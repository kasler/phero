package com.serjsmor.digitalpheromone;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.serjsmor.digitalpheromone.pheromones.Pheromone;
import com.serjsmor.digitalpheromone.pheromones.PicturePheromone;
import com.serjsmor.digitalpheromone.pheromones.TxtPheromone;

import java.util.ArrayList;

/**
 * Created by serjsmor on 1/1/15.
 */
public class MockupServer implements Server {
    private static final String TAG = "serj.pheromone.MockupServer";
    private User serjUser = new User("Serj");
    private User gfUser = new User("Your girlfriend");
    private User taliaUser = new User("Talia");
    // txt location
    private LatLng barIlanLocation = new LatLng(32.067512, 34.841403);
    private LatLng gfLocation = new LatLng(32.069249, 34.844171);
    private LatLng superLocation = new LatLng(32.067808, 34.846580);
    private LatLng carLocation = new LatLng(32.066991, 34.840653);
    private LatLng meetingLocation = new LatLng(32.063077, 34.769791);

    // photo location
    private LatLng barcelonaMuseumLocation = new LatLng(41.36844,2.15357);
    private LatLng austriaLocation = new LatLng(47.2302209, 11.4115058);
    private LatLng telAvivGraffitiLocation = new LatLng(32.0676462, 34.7712005);
    private LatLng bashaLocation = new LatLng(32.07246551, 34.8463583);
    private LatLng sinaiLocation = new LatLng(29.44333488, 34.84099388);
    private LatLng sosAustria = new LatLng(47.01326888, 10.28728008);
    private LatLng taliaSafariLocation = new LatLng(32.043668, 34.82516);

    private ArrayList<Pheromone> allPheromones = new ArrayList<>();
    @Override
    public boolean connect() {
        Log.d(TAG, "in connect()");
        return true;
    }

    @Override
    public void addPheromone(Pheromone phero) {
        Log.d(TAG, "in addPheromone");
        Log.d(TAG, "added pheromone : " + phero.toString());
    }

    @Override
    public ArrayList<Pheromone> getAllPheromone(LatLng fromLocation, double radius) {
        // txt
        allPheromones.add(new TxtPheromone(serjUser, barIlanLocation, R.drawable.tag_icon_team_mustard, "כולם מוזמנים ב20:00 להרצה של סטינג"));
        allPheromones.add(new TxtPheromone(gfUser, gfLocation, R.drawable.tag_icon_heart, "I miss you <3"));
        allPheromones.add(new TxtPheromone(serjUser, superLocation, R.drawable.tag_icon_team_blue, "אני בסופר, מה לקחת לפגישת מעבדה?"));
        allPheromones.add(new TxtPheromone(serjUser, carLocation, R.drawable.tag_icon_coins, "selling the Mazda 6 in the parking lot" +
                "number 18-666-19"));


        allPheromones.add(new PicturePheromone(serjUser, barcelonaMuseumLocation, R.drawable.tag_icon_public, R.drawable.photo_art));
        allPheromones.add(new PicturePheromone(serjUser, austriaLocation, R.drawable.tag_icon_public, R.drawable.photo_austria));
        allPheromones.add(new PicturePheromone(serjUser, telAvivGraffitiLocation, R.drawable.tag_icon_graffiti, R.drawable.photo_graffiti_tel_aviv));
        allPheromones.add(new PicturePheromone(serjUser, bashaLocation, R.drawable.tag_icon_heart, R.drawable.photo_looking_for_love));
        allPheromones.add(new PicturePheromone(serjUser, sinaiLocation, R.drawable.tag_icon_public, R.drawable.photo_sinai));
        allPheromones.add(new PicturePheromone(serjUser, sosAustria, R.drawable.tag_icon_sos, R.drawable.photo_sos_austria));
        allPheromones.add(new PicturePheromone(taliaUser, taliaSafariLocation, R.drawable.tag_icon_public, R.drawable.photo_talia_saffari));

        return allPheromones;
    }

    @Override
    public User getSerjUser() {
        return serjUser;
    }
}
