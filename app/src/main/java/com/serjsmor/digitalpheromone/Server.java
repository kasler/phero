package com.serjsmor.digitalpheromone;

import com.google.android.gms.maps.model.LatLng;
import com.serjsmor.digitalpheromone.pheromones.Pheromone;

import java.util.ArrayList;

/**
 * Created by serjsmor on 12/24/14.
 */
public interface Server {
    boolean connect();
    void addPheromone(Pheromone phero);
    ArrayList<Pheromone> getAllPheromone(LatLng location, double radius);
    User getSerjUser();
}
