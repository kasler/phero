package com.serjsmor.digitalpheromone;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by serjsmor on 1/1/15.
 */
public class User implements Parcelable{


    private String nickname;

    public User(String nickname) {
        this.nickname = nickname;
    }

    // should be the same order of the writing
    public  User(Parcel in) {
        this.nickname = in.readString();
    }

    public String getNickname() {
        return nickname;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nickname);
    }


    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {

        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

}
