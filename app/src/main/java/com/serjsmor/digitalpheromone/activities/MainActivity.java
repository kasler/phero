package com.serjsmor.digitalpheromone.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.serjsmor.digitalpheromone.MockupServer;
import com.serjsmor.digitalpheromone.fragments.MyMapFragment;
import com.serjsmor.digitalpheromone.R;
import com.serjsmor.digitalpheromone.Server;
import com.serjsmor.digitalpheromone.User;
import com.serjsmor.digitalpheromone.pheromones.Pheromone;

import java.util.ArrayList;


public class MainActivity extends Activity {

    private static final String TAG = "serj.pheromone.MainActivity";
    private Server server = new MockupServer();

    // ADD THE ROBOGUICE
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_main);

        User user = server.getSerjUser();
        ArrayList<Pheromone> pheromones = server.getAllPheromone(null, 0);

        if (savedInstanceState == null) {
            MyMapFragment myMapFragment = MyMapFragment.create(user, pheromones);
            this.getFragmentManager().beginTransaction()
                    .add(R.id.container, myMapFragment)
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
    }

    //
}

