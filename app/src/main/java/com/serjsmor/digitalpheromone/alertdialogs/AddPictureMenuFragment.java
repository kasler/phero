package com.serjsmor.digitalpheromone.alertdialogs;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.serjsmor.digitalpheromone.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddPictureMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddPictureMenuFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final int CAMERA = 0;
    private static final int GALLERY = 1;
    public static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final String TAG = "serj.pheromone.AddPictureMenuFragment";


    // TODO: Rename and change types of parameters
    private  Activity mActivity;
    private CharSequence[] mPhotoOptions = {"Take a photo", "Choose from Gallery"};
    private ImageView mImageView;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AddPictureMenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddPictureMenuFragment newInstance() {
        AddPictureMenuFragment fragment = new AddPictureMenuFragment();
//        Bundle args = new Bundle();
//        fragment.setArguments(args);
        return fragment;
    }

    public AddPictureMenuFragment() {
        // Required empty tag_icon_public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        Log.d(TAG, "creating AlertDialog");

        mActivity = getActivity();
        LayoutInflater inflater = mActivity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.alert_dialog_add_picture, null);
        mImageView = (ImageView) layout.findViewById(R.id.pheromone_picture_image);
        return new AlertDialog.Builder(mActivity)
                .setTitle("Add picture")
                .setView(layout)
                .setItems(mPhotoOptions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case CAMERA:
                                Log.d(TAG, "taking a photo");
                                dispatchTakePictureIntent();
                                addPhotoToGallery("");
                                break;
                            case GALLERY:
                                Log.d(TAG, "choosing photo from gallery");
                                break;
                        }
                    }
                })
                .create();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
           getTargetFragment().startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void addPhotoToGallery(String photoPath) {
    }

//    @Override
//    tag_icon_public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        super.onCreateView(inflater,container,savedInstanceState);
//
//        return ;
//    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        Log.d(TAG, "onActivityResult addPicture");
        super.onActivityResult(requestCode,resultCode,imageReturnedIntent);

        switch (requestCode) {
            case REQUEST_IMAGE_CAPTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Log.d(TAG, "in REQUEST_IMAGE_CAPTURE. RESULT_OK");
                    Bundle extras = imageReturnedIntent.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    mImageView.setImageBitmap(imageBitmap);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, imageReturnedIntent);
                }
                break;
        }
    }
}
