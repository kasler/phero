package com.serjsmor.digitalpheromone.alertdialogs;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.widget.EditText;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddTextMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddTextMenuFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match


    private static final String TAG = "serj.pheromone.EditTextMenuFragment";
    public static final String INPUT_TEXT_KEY = "INPUT_TEXT_KEY";

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment EditTextMenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddTextMenuFragment newInstance() {
        AddTextMenuFragment fragment = new AddTextMenuFragment();
//        Bundle args = new Bundle();
//        fragment.setArguments(args);
        return fragment;
    }

    public AddTextMenuFragment() {
        // Required empty tag_icon_public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        final Activity activity = getActivity();
        final EditText input = new EditText(activity);

        return new AlertDialog.Builder(activity)
        .setTitle("Enter your message to the world")
        .setMessage("")
        .setView(input)
        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Editable value = input.getText();
                Intent intent = activity.getIntent();
                intent.putExtra(INPUT_TEXT_KEY, value.toString());
                Log.d(TAG, "media type text input : " + value.toString());
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
                dismiss();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Do nothing.
            }
        }).create();

    }

    //    @Override
//    tag_icon_public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        TextView textView = new TextView(getActivity());
//        textView.setText(R.string.hello_blank_fragment);
//        return textView;
//    }


}
