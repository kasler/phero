package com.serjsmor.digitalpheromone.alertdialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.serjsmor.digitalpheromone.R;
import com.serjsmor.digitalpheromone.pheromones.Pheromone;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link } interface
 * to handle interaction events.
 * Use the {@link MediaMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MediaMenuFragment extends DialogFragment {
    private static final String TAG = "serj.pheromone.MeidaMenuFragment";

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MediaMenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MediaMenuFragment newInstance() {
        MediaMenuFragment fragment = new MediaMenuFragment();
//        Bundle args = new Bundle();
//        fragment.setArguments(args);
        return fragment;
    }

    public MediaMenuFragment() {
        // Required empty tag_icon_public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {


        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        final String[] mediaTypes = Pheromone.MediaEnum.names();
        int defaultChoice = 0;

        return new AlertDialog.Builder(getActivity())
            .setTitle(R.string.mediaBuilderTitle)
            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                    Pheromone.MediaEnum mEnum = Pheromone.MediaEnum.values()[selectedPosition];
                    Intent intent = getActivity().getIntent();
                    mEnum.attachTo(intent);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
                }
            })
            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Log.d(TAG, "inside isChecked media : ");
                }
            })
            .setSingleChoiceItems(mediaTypes, defaultChoice, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            }).create();
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }


}
