package com.serjsmor.digitalpheromone.alertdialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import com.serjsmor.digitalpheromone.R;
import com.serjsmor.digitalpheromone.pheromones.Pheromone;

import java.util.ArrayList;

/**
 * Created by serjsmor on 12/25/14.
 */
public class TagsMenuFragment extends DialogFragment {

    private static final String TAG = "serj.pheromone.MediaMenu";
    public static final String TAGS_STRING = "tags";

    public static TagsMenuFragment create(ArrayList<String> tags)
    {
        Bundle args = new Bundle();

        args.putStringArrayList(TAGS_STRING,tags);
        TagsMenuFragment menu = new TagsMenuFragment();
        menu.setArguments(args);

        return menu;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        final String[] tags = Pheromone.TagsEnum.names();
        final ArrayList<String> selectedTags = getArguments().getStringArrayList(TAGS_STRING);
        // arraylist to keep the selected items

        return new AlertDialog.Builder(getActivity())
                .setMultiChoiceItems(tags, null, new DialogInterface.OnMultiChoiceClickListener() {
                    // indexSelected contains the index of item (of which checkbox checked)
                    public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            // write your code when user checked the checkbox
                            //TODO save it in the savedPreferences and load it in onResume()
                            Log.d(TAG, "inside isChecked tags : " + tags[indexSelected]);
                            selectedTags.add(tags[indexSelected]);
                        } else if (selectedTags.contains(indexSelected)) {
                            // Else, if the item is already in the array, remove it
                            // write your code when user Uchecked the checkbox
                            selectedTags.remove(tags[indexSelected]);
                        }
                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                })

                .setTitle(R.string.tagBuilderTitle)
                .create();

    }

}
