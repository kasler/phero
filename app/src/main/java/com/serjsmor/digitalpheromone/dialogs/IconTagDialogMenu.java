package com.serjsmor.digitalpheromone.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.serjsmor.digitalpheromone.R;
import com.serjsmor.digitalpheromone.alertdialogs.AdapterWithIcons;
import com.serjsmor.digitalpheromone.pheromones.PheromoneController;

/**
 * Created by serjsmor on 1/10/15.
 */
public class IconTagDialogMenu extends Dialog implements AdapterView.OnItemClickListener {

    private static final String TAG = "serj.pheromoneController.IconTagDialogMenu";

    final String [] items = new String[] {"public info", "team work", "warning", "help me", "commerce", "love", "art"};
    final Integer[] icons = new Integer[] {R.drawable.tag_icon_public, R.drawable.tag_icon_team_blue, R.drawable.tag_icon_skull,
            R.drawable.tag_icon_sos, R.drawable.tag_icon_coins, R.drawable.tag_icon_heart, R.drawable.tag_icon_graffiti};

    private final Marker mMarker;
    private PheromoneController pheromoneController;


    public IconTagDialogMenu(Context context, Marker marker, PheromoneController pheromone) {
        super(context);
        this.mMarker = marker;
        this.pheromoneController = pheromone;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.icon_tag_layout);
        setTitle("Set image");
        setCancelable(false);
        ListAdapter adapter = new AdapterWithIcons(getContext(), items, icons);
        ListView listView = (ListView) findViewById(R.id.listview);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "inside onItemClick()");
        Integer resource = icons[position];
        BitmapDescriptor bitmap = BitmapDescriptorFactory.fromResource(resource);
        mMarker.setIcon(bitmap);
        pheromoneController.getPheromone().setImageResource(resource);
        this.dismiss();
    }
}
