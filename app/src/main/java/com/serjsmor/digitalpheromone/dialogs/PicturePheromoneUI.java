package com.serjsmor.digitalpheromone.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

import com.serjsmor.digitalpheromone.R;
import com.serjsmor.digitalpheromone.User;
import com.serjsmor.digitalpheromone.pheromones.Pheromone;
import com.serjsmor.digitalpheromone.pheromones.PheromoneController;
import com.serjsmor.digitalpheromone.pheromones.PicturePheromone;

/**
 * Created by serjsmor on 1/3/15.
 */
public class PicturePheromoneUI extends Dialog implements PheromoneController{
    private final PicturePheromone mPicturePheromone;

    public PicturePheromoneUI(Context context, PicturePheromone picPheromone) {
        super(context);
        this.mPicturePheromone = picPheromone;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_picture_pheromone);
        setCancelable(true);
        User user = mPicturePheromone.getmUser();
        String titleMessage = "Created by " + user.getNickname();
        setTitle(titleMessage);
        ImageView imageView = (ImageView) findViewById(R.id.pheromone_picture_image);
        Bitmap imageBitmap = mPicturePheromone.getImage();
        if (imageBitmap != null) {
            imageView.setImageBitmap(imageBitmap);
        }
        else {
            imageView.setImageResource(mPicturePheromone.getImageWrapper().get());
        }


        getWindow().setLayout(350,550);

    }

    @Override
    public Pheromone getPheromone() {
        return mPicturePheromone;
    }
}
