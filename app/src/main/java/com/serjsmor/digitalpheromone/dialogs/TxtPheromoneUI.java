package com.serjsmor.digitalpheromone.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import com.serjsmor.digitalpheromone.R;
import com.serjsmor.digitalpheromone.User;
import com.serjsmor.digitalpheromone.pheromones.Pheromone;
import com.serjsmor.digitalpheromone.pheromones.PheromoneController;
import com.serjsmor.digitalpheromone.pheromones.TxtPheromone;

/**
 * Created by User on 12/28/2014.
 */
public class TxtPheromoneUI extends Dialog implements PheromoneController {

    private TxtPheromone pheromone;


    public TxtPheromoneUI(Context context, TxtPheromone pheromone) {
        super(context);

        this.pheromone = pheromone;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_text_pheromone);
        setCancelable(true);
        User user = pheromone.getmUser();
        String titleMessage = "Created by " + user.getNickname();
        setTitle(titleMessage);
        TextView textView = (TextView) findViewById(R.id.pheromone_txt_view);
        String message = pheromone.getMessage();
        textView.setText(message);
        getWindow().setLayout(350,350);

    }

    @Override
    public Pheromone getPheromone() {
        return pheromone;
    }

}
