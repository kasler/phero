package com.serjsmor.digitalpheromone.pheromones;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.serjsmor.digitalpheromone.User;

import java.util.ArrayList;

/**
 * Created by serjsmor on 12/23/14.
 */
public abstract class Pheromone implements Parcelable {

    private User mUser;
    private LatLng mPosition;
    private ArrayList<String> mTags;
    private ResourceWrapper mImageResource;

    public ArrayList<String> getmTags() {
        return mTags;
    }

    public void setmTags(ArrayList<String> mTags) {
        this.mTags = mTags;
    }

    public Integer getImageResource() {
        return mImageResource.get();
    }

    public void setImageResource(Integer imageResource) {
        this.mImageResource = new ResourceWrapper(imageResource);
    }

    public LatLng getPosition() {
        return mPosition;
    }

    public void setPosition(LatLng mPosition) {
        this.mPosition = mPosition;
    }

    public void setmImageResource(ResourceWrapper mImageResource) {
        this.mImageResource = mImageResource;
    }

    public enum MediaEnum {
        TEXT(0),
        PICTURE(1);

        int value;
        private static final String name = MediaEnum.class.getName();

        MediaEnum(int value) {
            this.value = value;
        }

        public static String[] names() {
            MediaEnum[] states = values();
            String[] names = new String[states.length];

            for (int i = 0; i < states.length; i++) {
                names[i] = states[i].name();
            }

            return names;
        }


        public void attachTo(Intent intent) {
            intent.putExtra(name, ordinal());
        }
        public static MediaEnum detachFrom(Intent intent) {
            if(!intent.hasExtra(name)) throw new IllegalStateException();
            return values()[intent.getIntExtra(name, -1)];
        }
    }

    public enum TagsEnum {
        ART(0),
        EVENT(1),
        TEAMWORK(2),
        HELP(3),
        OFFER(4),
        PUBLIC(5);

        private int value;

        TagsEnum(int value) {
            this.value = value;
        }

        public static String[] names() {
            TagsEnum[] states = values();
            String[] names = new String[states.length];

            for (int i = 0; i < states.length; i++) {
                names[i] = states[i].name();
            }

            return names;
        }
    }

    public User getmUser() {
        return mUser;
    }

    public void setmUser(User mUser) {
        this.mUser = mUser;
    }

    public Pheromone(User user, LatLng position, Integer imageResource) {
        mUser = user;
        mImageResource = new ResourceWrapper(imageResource);
        mPosition = position;
        mTags = new ArrayList<String>();
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeParcelable(mUser, 0);
        out.writeParcelable(mPosition, 0);
        out.writeStringList(mTags);
        out.writeInt(mImageResource.get());
    }

    public Pheromone(Parcel in) {
        mUser = in.readParcelable(User.class.getClassLoader());
        mPosition = in.readParcelable(LatLng.class.getClassLoader());
        mTags = new ArrayList<>();
        in.readStringList(mTags);
        mImageResource = new ResourceWrapper(in.readInt());
    }

    public abstract PheromoneController createUI(Context context);
}
