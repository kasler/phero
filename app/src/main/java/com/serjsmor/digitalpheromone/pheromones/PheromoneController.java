package com.serjsmor.digitalpheromone.pheromones;

/**
 * Created by serjsmor on 1/10/15.
 */
public interface PheromoneController {
    public Pheromone getPheromone();
    public void show();
}
