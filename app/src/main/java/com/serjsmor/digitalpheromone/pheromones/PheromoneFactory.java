package com.serjsmor.digitalpheromone.pheromones;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.serjsmor.digitalpheromone.User;
import com.serjsmor.digitalpheromone.dialogs.PicturePheromoneUI;
import com.serjsmor.digitalpheromone.dialogs.TxtPheromoneUI;

/**
 * Created by serjsmor on 12/24/14.
 */
public class PheromoneFactory {

    // will have four methods
    // one for each type of pheromone (text,picture,sound,video)

    public static PheromoneController createTextPheromoneUI(User user, LatLng latLng, Integer resource, String message, Context context) {
        TxtPheromone txtPheromone = new TxtPheromone(user, latLng, resource, message);
        TxtPheromoneUI txtPheromoneUI = (TxtPheromoneUI) txtPheromone.createUI(context);

        return txtPheromoneUI;
    }


    public static PheromoneController createPicturePheromoneUI(User user, LatLng latLng, Integer resource, Bitmap imageBitmap, Context context) {
        PicturePheromone picturePheromone = new PicturePheromone(user, latLng, resource, imageBitmap);
        PicturePheromoneUI picturePheromoneUI = (PicturePheromoneUI) picturePheromone.createUI(context);

        return picturePheromoneUI;
    }
}
