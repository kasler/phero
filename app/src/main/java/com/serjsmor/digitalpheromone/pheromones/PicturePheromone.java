package com.serjsmor.digitalpheromone.pheromones;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.serjsmor.digitalpheromone.User;
import com.serjsmor.digitalpheromone.dialogs.PicturePheromoneUI;

/**
 * Created by serjsmor on 1/3/15.
 */
public class PicturePheromone extends Pheromone{

    private Bitmap image;

    private ResourceWrapper imageWrapper;

    public PicturePheromone(User user, LatLng latLng, Integer resource, Bitmap image) {
        super(user, latLng, resource);

        this.image = image;
        this.imageWrapper = new ResourceWrapper(0);
    }

    public PicturePheromone(User user, LatLng latLng, Integer resource, int drawable) {
        super(user, latLng, resource);

        imageWrapper = new ResourceWrapper(drawable);
    }

    public PicturePheromone(Parcel in) {
        super(in);
        image = in.readParcelable(Bitmap.class.getClassLoader());
        imageWrapper = new ResourceWrapper(in.readInt());
    }

    public Bitmap getImage() {
        return image;
    }


    public ResourceWrapper getImageWrapper() {
        return imageWrapper;
    }

    public void setImageWrapper(ResourceWrapper imageWrapper) {
        this.imageWrapper = imageWrapper;
    }

    @Override
    public PheromoneController createUI(Context context) {
        return new PicturePheromoneUI(context, this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeParcelable(image, 0);
        out.writeInt(imageWrapper.get());
    }

    public static final Parcelable.Creator<PicturePheromone> CREATOR = new Parcelable.Creator<PicturePheromone>() {
        public PicturePheromone createFromParcel(Parcel in) {
            return new PicturePheromone(in);
        }

        public PicturePheromone[] newArray(int size) {
            return new PicturePheromone[size];
        }
    };
}
