package com.serjsmor.digitalpheromone.pheromones;

/**
 * Created by serjsmor on 1/10/15.
 */
public class ResourceWrapper {
    private Integer resource;

    public ResourceWrapper(Integer resource) {
        this.resource = resource;
    }

    public void set(Integer resource) {
        this.resource = resource;
    }

    public Integer get() {
        return this.resource;
    }
}
