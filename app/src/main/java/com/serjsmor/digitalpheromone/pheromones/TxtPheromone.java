package com.serjsmor.digitalpheromone.pheromones;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.serjsmor.digitalpheromone.User;
import com.serjsmor.digitalpheromone.dialogs.TxtPheromoneUI;

/**
 * Created by serjsmor on 12/24/14.
 */
public class TxtPheromone extends Pheromone {

    private final String mMessage;

    public TxtPheromone(User user,LatLng latLng, Integer resource, String message) {
        super(user, latLng, resource);

        this.mMessage = message;

    }

    public String getMessage() {
        return mMessage;
    }

    @Override
    public PheromoneController createUI(Context context) {
        return new TxtPheromoneUI(context, this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public TxtPheromone(Parcel in) {
        super(in);
        mMessage = in.readString();
    }

    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeString(mMessage);
    }

    public static final Parcelable.Creator<TxtPheromone> CREATOR = new Parcelable.Creator<TxtPheromone>() {
        public TxtPheromone createFromParcel(Parcel in) {
            return new TxtPheromone(in);
        }

        public TxtPheromone[] newArray(int size) {
            return new TxtPheromone[size];
        }
    };
}
