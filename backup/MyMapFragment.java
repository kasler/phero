package com.serjsmor.digitalpheromone.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.serjsmor.digitalpheromone.R;
import com.serjsmor.digitalpheromone.User;
import com.serjsmor.digitalpheromone.alertdialogs.AddPictureMenuFragment;
import com.serjsmor.digitalpheromone.alertdialogs.AddTextMenuFragment;
import com.serjsmor.digitalpheromone.alertdialogs.MediaMenuFragment;
import com.serjsmor.digitalpheromone.alertdialogs.TagsMenuFragment;
import com.serjsmor.digitalpheromone.dialogs.IconTagDialogMenu;
import com.serjsmor.digitalpheromone.pheromones.Pheromone;
import com.serjsmor.digitalpheromone.pheromones.PheromoneController;
import com.serjsmor.digitalpheromone.pheromones.PheromoneFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by serjsmor on 12/24/14.
 */
public class MyMapFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "serj.pheromone.MyMapFragment";

    private static final int MEDIA_DIALOG_RETURN_CODE = 100;
    private static final int ADD_TEXT_DIALOG_RETURN_CODE = 101;
    private static final int TAGS_DIALOG_RETURN_CODE = 102;
    private static final int ADD_PICTURE_DIALOG_RETURN_CODE = 103;
    private static final int ADD_ICON_TAG_DIALOG_RETURN_CODE = 104;

    private static final float ZOOM_LEVEL = 14;

    private static final String LOCATION_KEY = "LOCATION_KEY";
    private static final String USER_KEY = "USER_KEY";
    private static final String MAP_STATE_KEY = "MAP_STATE_KEY";
    private static final String PHEROMONES_KEY = "PHEROMONES_KEY";


    private HashMap<Marker, PheromoneController> mAllPheromoneControllers = new HashMap<>();
    private ArrayList<Pheromone> mAllPheromones = new ArrayList<>();
    private HashMap<Marker, PheromoneController> mMyPheromoneControllers = new HashMap<>();
    private AddPictureMenuFragment addPictureMenuFragment;
    private User mUser;
    private GoogleMap mMap;
    private MapView mMapView;
    private Activity mActivity;
    private Location mLastLocation;
    private LatLng mLastLatLng;
    private Bundle mMapState;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(TAG, "onAttach");
        mActivity = activity;
    }

    public static MyMapFragment create(User user, ArrayList<Pheromone> pheromones) {
        Log.d(TAG, "static create");
        Bundle args = new Bundle();
        args.putParcelable(USER_KEY, user);
        args.putParcelableArrayList(PHEROMONES_KEY, pheromones);
        MyMapFragment mapFragment = new MyMapFragment();
        mapFragment.setArguments(args);
        return mapFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        if (savedInstanceState != null) {
            Log.d(TAG, "savedInstanceState != null");
            mUser = savedInstanceState.getParcelable(USER_KEY);
            mLastLocation = savedInstanceState.getParcelable(LOCATION_KEY);
            mLastLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            mAllPheromones = savedInstanceState.getParcelableArrayList(PHEROMONES_KEY);
            // map bundle
            mMapState = new Bundle();
            mMapState.putBundle(MAP_STATE_KEY, savedInstanceState.getBundle(MAP_STATE_KEY));

        }
        else { // started first time from activity
            Log.d(TAG, "savedInstanceState == null");
            Bundle bundle = getArguments();
            mUser = bundle.getParcelable(USER_KEY);
            mAllPheromones = bundle.getParcelableArrayList(PHEROMONES_KEY);
            MapsInitializer.initialize(this.mActivity);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        Log.d(TAG, "onCreateView");
        View v = inflater.inflate(R.layout.fragment_main_map, container, false);

        // TODO getAllMarkers from server
        v.findViewById(R.id.record_button).setOnClickListener(this);

        mMapView = (MapView) v.findViewById(R.id.mapview);
        mMapView.onCreate(mMapState);


        mMap = mMapView.getMap();
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setMyLocationEnabled(true);

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls



        // TODO can move to a private class
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                PheromoneController pheromoneController = mAllPheromoneControllers.get(marker);
                Pheromone pheromone = pheromoneController.getPheromone();
                Log.d(TAG, "showing pheromone resource ui " + pheromone.getImageResource());
                pheromoneController.show();
                return true;
            }
        });
        // TODO can move to a private class
        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            public void onMyLocationChange(Location arg0) {
                // first time
//                if (mLastLocation == null ) {
                    mLastLocation = arg0;
                    mLastLatLng = new LatLng(arg0.getLatitude(), arg0.getLongitude());
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(mLastLatLng)      // Sets the center of the map to Mountain View
                            .zoom(ZOOM_LEVEL)                   // Sets the zoom
                            .build();                   // Creates a CameraPosition from the builder
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                }

                // other times need to think about
            }
        });
        // TODO can move to a private class
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                PheromoneController controller = mAllPheromoneControllers.get(marker);
                controller.getPheromone().setPosition(marker.getPosition());
            }
        });

        // init all markers and controllers
        restorePheromoneControllersAndMarkers(mAllPheromones);

        return v;
    }

    private void restorePheromoneControllersAndMarkers(ArrayList<Pheromone> mAllPheromones) {
        for (Pheromone pheromone : mAllPheromones) {
            if (mMap == null) {
                Log.d(TAG, "mMap is null");
                setUpMapIfNeeded();
            }
            PheromoneController controller = pheromone.createUI(mActivity);

            // fix this shit using some parceling the right moment
            boolean draggable = (mUser.getNickname().equals(pheromone.getmUser().getNickname())) ? true : false;
            MarkerOptions options;
            if (pheromone.getImageResource() != null) {
                BitmapDescriptor descriptor = BitmapDescriptorFactory.fromResource(pheromone.getImageResource());
                options = new MarkerOptions()
                        .position(pheromone.getPosition())
                        .icon(descriptor)
                        .draggable(draggable);
            }
            else {
                options = new MarkerOptions()
                        .position(pheromone.getPosition())
                        .draggable(draggable);
            }

            if (options == null) {
                Log.d(TAG, "options is null ***");
            }

            Marker marker = null;
            if (mMap != null) {
                marker = mMap.addMarker(options);
            }



            mAllPheromoneControllers.put(marker, controller);
            boolean sameUser = draggable;
            if (sameUser) {
                mMyPheromoneControllers.put(marker, controller);
            }

        }
    }



    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        mMapView.onResume();
        setUpMapIfNeeded();

    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
        mMapView.onPause();
        mMap = null;
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            Log.d(TAG, "setMapIfNeeded");
            mMap = ((MapView) getView().findViewById(R.id.mapview)).getMap();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    @Override
    public void onClick(View v) {
        switch(v.getId())
        {

            case R.id.record_button:
            {
                mediaAlertDialog();
            }
        }

    }

    private void tagAlertDialog(Pheromone pheromone) {
        TagsMenuFragment tagsMenu = TagsMenuFragment.create(pheromone.getmTags());
        tagsMenu.setTargetFragment(this, TAGS_DIALOG_RETURN_CODE);
        tagsMenu.show(getFragmentManager(), "tagsAlertDialog");

    }

    private void mediaAlertDialog() {
        MediaMenuFragment mediaMenu = MediaMenuFragment.newInstance();
        mediaMenu.setTargetFragment(this, MEDIA_DIALOG_RETURN_CODE);
        mediaMenu.show(getFragmentManager(), "mediaAlertDialog");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case MEDIA_DIALOG_RETURN_CODE:
                Log.d(TAG, "in MEDIA_DIALOG_RETURN");
                Pheromone.MediaEnum mediaType = Pheromone.MediaEnum.detachFrom(data);

                switch (mediaType)
                {
                    case TEXT:
                        addTextAlertDialog();
                        break;

                    case PICTURE:
                        addPictureAlertDialog();
                        break;
                }

                break;

            case ADD_TEXT_DIALOG_RETURN_CODE:
                Log.d(TAG, "in EDIT_DIALOG_RETURN");
                // string from the menu
                String txtData = data.getStringExtra(AddTextMenuFragment.INPUT_TEXT_KEY);
                PheromoneController txtPheromone = PheromoneFactory.createTextPheromoneUI(mUser, mLastLatLng, null, txtData, mActivity);
                finishPheromoneCreation(txtPheromone);
                break;

//            case ADD_PICTURE_DIALOG_RETURN_CODE:
//                Log.d(TAG, "in ADD_PICTURE_RETURN");
//                break;

            case TAGS_DIALOG_RETURN_CODE:
                Log.d(TAG, "in TAGS_DIALOG_RETURN");
                break;

            case AddPictureMenuFragment.REQUEST_IMAGE_CAPTURE:
                Log.d(TAG, "in REQUEST_IMAGE_CAPTURE");
                if (resultCode == Activity.RESULT_OK) {
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    PheromoneController picturePheromone = PheromoneFactory.createPicturePheromoneUI(mUser, mLastLatLng, 0, imageBitmap, mActivity);
                    finishPheromoneCreation(picturePheromone);
                }
                else
                {
                    Log.d(TAG, "REQUEST_IMAGE_CAPTURE failed");
                }
                break;

            case ADD_ICON_TAG_DIALOG_RETURN_CODE:
                Log.d(TAG, "in ADD_ICON_TAG_DIALOG_RETURN_CODE");
                break;

        }

    }

    private void finishPheromoneCreation(PheromoneController pheromone) {
//        pheromoneToBe = pheromone;
        // TODO addPheromone to server
        Marker marker = createMarker(true, mLastLatLng, null); // should be some default resource ? (third null parameter)
        mAllPheromoneControllers.put(marker, pheromone);
        mMyPheromoneControllers.put(marker, pheromone);
        mAllPheromones.add(pheromone.getPheromone());
        IconTagDialogMenu menu = new IconTagDialogMenu(mActivity, marker, pheromone);
        menu.show();
        Log.d(TAG, "After menu.show()");
    }


    private void addPictureAlertDialog() {
        addPictureMenuFragment = AddPictureMenuFragment.newInstance();
        addPictureMenuFragment.setTargetFragment(this, ADD_PICTURE_DIALOG_RETURN_CODE);
        addPictureMenuFragment.show(getFragmentManager(), "ADD_PICTURE_MENU_FRAGMENT");
    }


    private Marker createMarker(boolean draggable, LatLng position, BitmapDescriptor icon) {
        MarkerOptions options = new MarkerOptions();
        options.position(position);
        options.draggable(draggable);
//        options.icon();
        return mMap.addMarker(options);
    }

    private void addTextAlertDialog() {
        AddTextMenuFragment editFrag = AddTextMenuFragment.newInstance();
        editFrag.setTargetFragment(this, ADD_TEXT_DIALOG_RETURN_CODE);
        editFrag.show(getFragmentManager(), "EDIT_TEXT_MENU_FRAGMENT");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);

        Log.d(TAG, "onSaveInstanceState");

        outState.putParcelable(LOCATION_KEY, mLastLocation);
        outState.putParcelable(USER_KEY, mUser);
        outState.putParcelableArrayList(PHEROMONES_KEY, mAllPheromones);
    }

}